<!DOCTYPE html>
<html>
    <head>
        <title>Contact Us | Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Contact Us</div>

                    @if(Session::has('message'))
                        <div role="alert">
                            <b>{{ Session::get('message') }}</b>
                        </div>
                    @endif

                    @include('errors/list')

                    <form class="form" role="form" method="POST" action="{{ url('/contact-us') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <label><b>Name</b></label>
                        <input type="text" class="form-control" name="name">

                        <br/>

                        <label><b>E-Mail Address</b></label>
                        <input type="email" class="form-control" name="email">

                        <br/>

                        <button type="submit">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
